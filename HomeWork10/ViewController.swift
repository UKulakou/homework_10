//
//  ViewController.swift
//  HomeWork10
//
//  Created by Uladzimir Kulakou on 6/19/21.
//  Copyright © 2021 Uladzimir Kulakou. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var progressViewOutlet: UIProgressView!
    @IBOutlet weak var textFieldOutlet: UITextField!
    @IBOutlet weak var sliderOutlet: UISlider!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var swithOutlet: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        // Do any additional setup after loading the view.
    }

    @IBAction func sliderAction(_ sender: UISlider) {
        progressViewOutlet.progress = sender.value / 100
        let shortValue = round(Double(sender.value) * 10) / 10
        textFieldOutlet.text = String(shortValue)
    }
    @IBAction func textFieldAction(_ sender: Any) {
        if let numberOfField = textFieldOutlet.text,
            let numder = Float(numberOfField) {
            sliderOutlet.value = numder
            progressViewOutlet.progress = numder / 100
        } else {
            timeLable.text = "Fuck"
        }
    }
    private func setupUI() {
        sliderOutlet.value = 50
        textFieldOutlet.placeholder = String(sliderOutlet.value / 100)
        timeLable.backgroundColor = .gray
        swithOutlet.isOn = false
    }

    @IBAction func setTime(_ sender: UIButton) {
        let time = String(datePicker.calendar.component(.hour, from: datePicker.date))
        let min = String(datePicker.calendar.component(.minute, from: datePicker.date))
        timeLable.text = "\(time) : \(min)"
    }

    @IBAction func switchAction(_ sender: Any) {
        if swithOutlet.isOn {
            timeLable.backgroundColor = .green
        } else {
            timeLable.backgroundColor = .gray
        }
    }

    @IBAction func clearAction(_ sender: Any) {
        swithOutlet.isOn = false
        timeLable.text = nil
        timeLable.backgroundColor = .gray
        timeLable.text = "Set time"
    }

}

